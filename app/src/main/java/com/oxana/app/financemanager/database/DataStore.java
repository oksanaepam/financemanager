package  com.oxana.app.financemanager.database;

import java.util.Set;

import  com.oxana.app.financemanager.Util.LoginException;
import  com.oxana.app.financemanager.models.Account;
import  com.oxana.app.financemanager.models.Record;
import  com.oxana.app.financemanager.models.User;

/**
 * Created by mac on 27.07.2014.
 */
public interface DataStore {
    // return null if no such user
    User getUser(String name);

    // If no users, return empty collection (not null)
    Set<String> getUserNames();

    // If no accounts, return empty collection (not null)
    Set<Account> getAccounts(User owner);

    // If no records, return empty collection (not null)
    Set<Record> getRecords(Account account);

    void addUser(User user);

    void addAccount(User user, Account account);

    void addRecord(Account account, Record record);

    // return removed User or null if no such user
    User removeUser(String name);

    // return null if no such account
    Account removeAccount(User owner, Account account);

    // return null if no such record
    Record removeRecord(Account from, Record record);

    void addLogInfo(String userName, String password) throws LoginException;

    boolean matchPassword(String userName, String password) throws LoginException;
}

