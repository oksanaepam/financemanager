package  com.oxana.app.financemanager.models;

/**
 * Created by Oksana_Skorniakova on 8/5/2014.
 */
public class Category {

    private String mName;
    private String mDescription;

    public Category(String name, String description){
        this.mName=name;
        this.mDescription=description;
    }

    public Category(String name){
        this(name, null);
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
