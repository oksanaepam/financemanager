package  com.oxana.app.financemanager.models;

import android.util.Log;

import java.text.ParseException;
import java.util.Comparator;

import  com.oxana.app.financemanager.Util.DateUtil;

/**
 * Created by mac on 27.07.2014.
 */
public class Record {

    private long mId;
    private String mUser;
    private long mAccount;
    private OperationType mType;
    private String mDate;
    private Double mSum;
    private String mDescription;
    private Category mCategory;

    private final String TAG = this.getClass().toString();


    public Record(long id, String user, long account, OperationType type, String date, Double sum, Category category, String desc) {
        this.mId = id;
        this.mUser = user;
        this.mAccount = account;
        this.mType = type;
        this.mDate = date;
        this.mSum = sum;
        this.mDescription = desc;
        this.mCategory = category;
    }


    public Record(long id, String user, long account, OperationType type, String date, Double sum, Category category) {
        this(id, user, account, type, date, sum, category, "");
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public Double getSum() {
        return mSum;
    }

    public void setSum(Double mSum) {
        this.mSum = mSum;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public OperationType getType() {
        return mType;
    }

    public void setType(OperationType mType) {
        this.mType = mType;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Record) {
                if (this.toString().equals(((Record) o).toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    /*@Override
    public int hashCode() {
        return mUser.concat(mAccount).concat(mDate).hashCode();
    }

    @Override
    public String toString() {
        return mUser.concat(mAccount).concat(mDate).toString();
    }*/

    public String getUser() {
        return mUser;
    }

    public long getAccount() {
        return mAccount;
    }

    public Category getCategory(){
        return mCategory;
    }

    public long getId() {
        return mId;
    }

    public void setCategory(Category mCategory) {
        this.mCategory = mCategory;

    }

    public void setId(long mId) {
        this.mId = mId;
    }


    public static enum OperationType {
        IN, OUT
    }

    public class RecordComparator implements Comparator<Record> {

        @Override
        public int compare(Record r1, Record r2) {
            long d1 = 0;
            long d2 = 0;
            try {
                d1 = DateUtil.getDate(r1.getDate()).getTime();
                d2 = DateUtil.getDate(r2.getDate()).getTime();
            } catch (ParseException e) {
                Log.e(TAG, e.toString());
            }

            if (d1 != 0 && d2 != 0) {
                if (d1 < d2) return 1;
                if (d1 == d2) {
                    if (!r1.equals(r2)) {
                        return r1.toString().compareTo(r2.toString());
                    } else return 0;
                }
                if (d1 > d2) return -1;

            }
            return r1.toString().compareTo(r2.toString());

        }
    }
}
