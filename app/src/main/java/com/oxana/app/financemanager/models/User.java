package  com.oxana.app.financemanager.models;

import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Created by mac on 27.07.2014.
 */
public class User {

    private String mName;
    private Set<Account> mSetAccount;

    private final String TAG=this.getClass().toString();

    public User(String name, Set<Account> set){
        this.mName=name;
        this.mSetAccount=set;
    }

    public User(String name){
        this.mName=name;
        mSetAccount= new HashSet();
    }


    public void setName(String name){
        this.mName=name;
    }

    public void setSet(Set<Account> set){
        this.mSetAccount=set;
    }

    public String getName(){
        return mName;
    }

    public Set<Account> getSet(){


        return mSetAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (o!=null) {
            if (o instanceof User) {
                if (mName.equals(((User) o).getName())) {
                        return true;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return mName.hashCode();
    }

    @Override
    public String toString() {
        return mName.toString();
    }
}
