package  com.oxana.app.financemanager.database;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import  com.oxana.app.financemanager.Util.DateUtil;
import  com.oxana.app.financemanager.Util.LoginException;
import  com.oxana.app.financemanager.models.Account;
import  com.oxana.app.financemanager.models.Record;
import  com.oxana.app.financemanager.models.User;

/**
 * Created by mac on 28.07.2014.
 */
public class DataInCollections implements DataStore {

    private Map<String, String> mLoginInfo;
    private Map<String, User> mUsers;
    private Map<String, Set<Account>> mAccounts;
    private Map<Long, Set<Record>> mRecords;

/*    private void initDummy() {
        String userName = "helloworld";
        String pass = "qwerty";
        long accountName1 = 1;
        long accountName2 = 2;
        long recordId1 = 1;
        long recordId2 = 2;
        long recordId3 = 3;
        long recordId4 = 4;


        mLoginInfo.put(userName, pass);
        Record record1 = new Record(recordId1,userName, accountName1, Record.OperationType.IN, DateUtil.getDate(), 1000.0);
        Record record2 = new Record(recordId2,userName, accountName1, Record.OperationType.OUT, DateUtil.getDate(), 5000.0);

        HashSet<Record> rec1 = new HashSet();
        rec1.add(record1);
        rec1.add(record2);
        mRecords.put(accountName1, rec1);

        Record record3 = new Record(recordId3, userName, accountName2, Record.OperationType.IN, DateUtil.getDate(), 200.0);
        Record record4 = new Record(recordId4, userName, accountName2, Record.OperationType.OUT, DateUtil.getDate(), 300.0);

        HashSet<Record> rec2 = new HashSet();
        rec2.add(record3);
        rec2.add(record4);
        mRecords.put(accountName2, rec2);


        Account account1 = new Account(accountName1, userName,  10000.0, rec1);
        Account account2 = new Account(accountName1, userName, 5000.0, rec2);
        HashSet<Account> acc = new HashSet();
        acc.add(account1);
        acc.add(account2);
        mAccounts.put(userName, acc);


        User user = new User(userName, acc);
        mUsers.put(userName, user);
    }*/

    public DataInCollections() {
        mUsers = new HashMap();
        mLoginInfo = new HashMap();
        mRecords = new HashMap();
        mAccounts = new HashMap();
        mRecords = new HashMap();

        //initDummy();
    }

    private final String TAG = this.getClass().toString();

    @Override
    public User getUser(String name) {
        return mUsers.get(name);
    }

    @Override
    public Set<String> getUserNames() {
        return mUsers.keySet();
    }

    @Override
    public Set<Account> getAccounts(User owner) {
        return mAccounts.get(owner);
    }

    @Override
    public Set<Record> getRecords(Account account) {
        return mRecords.get(account);
    }

    @Override
    public void addUser(User user) {
        mUsers.put(user.getName(), user);
    }

    @Override
    public void addAccount(User user, Account account) {
        mAccounts.get(user.getName()).add(account);

    }

    @Override
    public void addRecord(Account account, Record record) {
        mRecords.get(account.getId()).add(record);
    }

    @Override
    public User removeUser(String name) {
        return mUsers.remove(name);
    }

    @Override
    public Account removeAccount(User owner, Account account) {
        if (mAccounts.get(owner.getName()).remove(account)) {
            return account;
        } else {
            return null;
        }
    }

    @Override
    public Record removeRecord(Account from, Record record) {
        if (mRecords.get(from.getId()).remove(record)) {
            return record;
        } else {
            return null;
        }
    }

    @Override
    public void addLogInfo(String userName, String password) throws LoginException {
        String value = mLoginInfo.get(userName);

        if (value != null) {
            new LoginException("There is already such user");
        } else {
            mLoginInfo.put(userName, password);
        }
    }

    @Override
    public boolean matchPassword(String userName, String password) throws LoginException {
        String value = mLoginInfo.get(userName);

        if (value == null) {
            new LoginException("There is no such user");
        } else if (value.equals(password)) {
            return true;
        }
        return false;
    }
}
