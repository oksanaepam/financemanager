package  com.oxana.app.financemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import  com.oxana.app.financemanager.models.Account;
import  com.oxana.app.financemanager.models.Category;
import  com.oxana.app.financemanager.models.Record;
import  com.oxana.app.financemanager.models.User;

/**
 * Created by Oksana_Skorniakova on 8/5/2014.
 */
public class SqlAdapter {

    private Cursor cursor;
    private SQLiteDatabase database;
    private DbHelper dbOpenHelper;
    private Context context;


    private final String TAG = this.getClass().toString();

    private final String DB_NAME = "MY_DB";
    private final int DB_VESION = 1;


    public SqlAdapter(Context context) {
        super();
        this.context = context;
        init();
    }

    private void init() {
        dbOpenHelper = new DbHelper(context, DB_NAME, null, DB_VESION);
        try {
            database = dbOpenHelper.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, "Error while getting database");
        }
    }

    public Cursor getAll(String className) {

        String table = selectTable(className);
        if (table.equals(null)) {
            return null;
        }
        Cursor cursor = database.rawQuery("select * from " + table, null);
        return cursor;
    }

    private String selectTable(String className) {
        String table;
        if (className.equals(User.class.getName())) {
            table = DbHelper.TABLE_USERS;
        } else if (className.equals(Account.class.getName())) {
            table = DbHelper.TABLE_ACCOUNTS;
        } else if (className.equals(Record.class.getName())) {
            table = DbHelper.TABLE_RECORDS;
        } else if (className.equals(Category.class.getName())) {
            table = DbHelper.TABLE_CATEGORY;
        } else {
            return null;
        }
        return table;
    }

    private String getName(Object id) {
        try {
            String str = (String) id;
            return str;
        } catch (Exception er) {
            Log.e(TAG, "Wrong id");
            //todo set error to screen
        }
        return null;
    }


    public Cursor getElement(String className, String id) {

        String table = selectTable(className);
        if (table.equals(null)) {
            return null;
        }
        return database.query(table, null, "ID" + " = '" + id + "'", null, null, null, null);
    }


    public Cursor getAccounts(String userName) {

        return database.query(DbHelper.TABLE_ACCOUNTS, null, "TABLE_ACCOUNTS_KEY_USER" + " = '" + userName + "'", null, null, null, null);
    }

    public Cursor getRecords(Long accoutId) {

        return database.query(DbHelper.TABLE_RECORDS, null, "TABLE_RECORDS_KEY_ACCOUNT" + " = '" + accoutId.toString() + "'", null, null, null, null);
    }

    public Long addElement(String className, ContentValues values) {
        String table = selectTable(className);
        if (table.equals(null)) {
            return null;
        }
        return database.insert(table, null, values);
    }

    public void  addElement(String className, ContentValues values, Long id) {
        if (!(className.equals(Account.class.getName()) && className.equals(Record.class.getName()))) {
            String table = selectTable(className);
            if (table.equals(null)) {
                return;
            }
            database.update(table, values, "ID = " + id, null);
        } else {
            return;
        }
    }


    public int  removeElement(String className, String where) {
        String table = selectTable(className);
        if (table.equals(null)) {
            return 0;
        }
        return database.delete(table,"ID = " + where, null);
    }
}



