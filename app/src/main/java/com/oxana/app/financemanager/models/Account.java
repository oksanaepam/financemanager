package  com.oxana.app.financemanager.models;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mac on 27.07.2014.
 */
public class Account {

    private long mId;
    private Double mBalance;
    private Set<Record> mSetRecords;
    private String mUser;

    private final String TAG=this.getClass().toString();

    public Account(long id, String user, Double balance, Set<Record> set ){
        this.mId=id;
        this.mUser=user;
        this.mBalance=balance;
        if (set==null){
            this.mSetRecords=new HashSet();
        } else {
            this.mSetRecords=set;
        }
    }

    public Account(long id, String user, Double balance ){
        this(id, user, balance, null);
    }

    public Account(long id, String user){
        this(id, user, 0.0, null);
    }

    public Set<Record> getSetRecords() {
        return mSetRecords;
    }

    public void setSetRecords(Set<Record> Set) {
        this.mSetRecords = Set;
    }

    public Double getBalance() {
        return mBalance;
    }

    public void setBalance(Double balance) {
        this.mBalance = balance;
    }

    public long getId() {
        return mId;
    }

     @Override
    public boolean equals(Object o) {
        if (o!=null) {
            if (o instanceof Account) {
                if (this.toString().equals(((Account) o).toString())) {
                                return true;
                }
            }
        }
        return false;
    }

 /*   @Override
    public int hashCode() {
        return mUser.concat(mName).hashCode();
    }

    @Override
    public String toString() {
        return mUser.concat(mName).toString();
    }*/

    public String getUser() {
        return mUser;
    }

}
