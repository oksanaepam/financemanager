package  com.oxana.app.financemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.HashSet;
import java.util.Set;

import  com.oxana.app.financemanager.Util.LoginException;
import  com.oxana.app.financemanager.models.Account;
import  com.oxana.app.financemanager.models.Category;
import  com.oxana.app.financemanager.models.Record;
import  com.oxana.app.financemanager.models.User;

/**
 * Created by Oksana_Skorniakova on 8/5/2014.
 */
public class DataInDb implements DataStore {

    private Context mContext;
    private SqlAdapter mAdapter;

    public DataInDb(Context context) {
        this.mContext = context;
        mAdapter = new SqlAdapter(mContext);
    }

    @Override
    public User getUser(String name) {
        Cursor cursor = mAdapter.getElement(User.class.getName(), name);
        User user = new User(cursor.getString(DbHelper.TABLE_USERS_COLUMN_NAME));
        user.setSet(getAccounts(user));
        return user;
    }

    @Override
    public Set<String> getUserNames() {
        Set<String> set = new HashSet();
        Cursor cursor = mAdapter.getAll(User.class.getName());
        if (cursor.moveToFirst()) {
            do {
                set.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return set;
    }

    @Override
    public Set<Account> getAccounts(User owner) {
        Set<Account> set = new HashSet();
        Cursor cursor = mAdapter.getAccounts(owner.getName());

        if (cursor.moveToFirst()) {
            do {
                Account account = new Account(
                        cursor.getLong(DbHelper.TABLE_ACCOUNTS_COLUMN_ID),
                        cursor.getString(DbHelper.TABLE_ACCOUNTS_COLUMN_USER),
                        cursor.getDouble(DbHelper.TABLE_ACCOUNTS_COLUMN_BALANCE),
                        null);

                account.setSetRecords(getRecords(account));
                set.add(account);

            } while (cursor.moveToNext());
        }
        return set;
    }

    @Override
    public Set<Record> getRecords(Account account) {
        Set<Record> set = new HashSet();
        Cursor cursor = mAdapter.getRecords(account.getId());

        if (cursor.moveToFirst()) {
            do {
                Record record = new Record(
                        cursor.getLong(DbHelper.TABLE_RECORDS_COLUMN_ID),
                        cursor.getString(DbHelper.TABLE_RECORDS_COLUMN_USER),
                        cursor.getLong(DbHelper.TABLE_RECORDS_COLUMN_ACCOUNT),
                        (cursor.getInt(DbHelper.TABLE_RECORDS_COLUMN_TYPE) > 0 ? Record.OperationType.OUT : Record.OperationType.IN),
                        cursor.getString(DbHelper.TABLE_RECORDS_COLUMN_DATE),
                        cursor.getDouble(DbHelper.TABLE_RECORDS_COLUMN_SUM),
                        getCategory(cursor.getString(DbHelper.TABLE_RECORDS_COLUMN_CATEGORY)),
                        cursor.getString(DbHelper.TABLE_RECORDS_COLUMN_DESCRIPTION)
                );
                set.add(record);

            } while (cursor.moveToNext());
        }
        return set;
    }

    private Category getCategory(String name) {
        return null;
    }

    @Override
    public void addUser(User user) {
        ContentValues values = new ContentValues();

        values.put(DbHelper.TABLE_USERS_KEY_NAME, user.getName());
        mAdapter.addElement(User.class.getName(), values);

        for (Account account : user.getSet()) {
            addAccount(user, account);
        }

    }

    @Override
    public void addAccount(User user, Account account) {

        ContentValues values = new ContentValues();

        values.put(DbHelper.TABLE_ACCOUNTS_KEY_ID, account.getId());
        values.put(DbHelper.TABLE_ACCOUNTS_KEY_USER, user.getName());
        values.put(DbHelper.TABLE_ACCOUNTS_KEY_BALANCE, account.getBalance());

        mAdapter.addElement(Account.class.getName(), values);

        for (Record record : account.getSetRecords()) {
            addRecord(account, record);
        }
    }

    @Override
    public void addRecord(Account account, Record record) {

        ContentValues values = new ContentValues();

        values.put(DbHelper.TABLE_RECORDS_KEY_ID, record.getId());
        values.put(DbHelper.TABLE_RECORDS_KEY_ACCOUNT, account.getId());
        values.put(DbHelper.TABLE_RECORDS_KEY_SUM, record.getSum());
        values.put(DbHelper.TABLE_RECORDS_KEY_DATE, record.getDate());
        values.put(DbHelper.TABLE_RECORDS_KEY_TYPE, (record.getType() == Record.OperationType.OUT ? 0 : 1));
        values.put(DbHelper.TABLE_RECORDS_KEY_USER, record.getUser());
        values.put(DbHelper.TABLE_RECORDS_KEY_CATEGORY, record.getCategory().getName());
        values.put(DbHelper.TABLE_RECORDS_KEY_DESCRIPTION, record.getDescription());

        mAdapter.addElement(Record.class.getName(), values);
    }

    @Override
    public User removeUser(String name) {
        User user = new User(name);
        mAdapter.removeElement(User.class.getName(), name);
        for (Account account : user.getSet()) {
            removeAccount(user, account);
        }
        return user;
    }

    @Override
    public Account removeAccount(User owner, Account account) {
        mAdapter.removeElement(Account.class.getName(), String.valueOf(account.getId()));
        for (Record record : account.getSetRecords()) {
            removeRecord(account, record);
        }
        return account;
    }

    @Override
    public Record removeRecord(Account from, Record record) {
        mAdapter.removeElement(Account.class.getName(), String.valueOf(record.getId()));
        return record;
    }

    @Override
    public void addLogInfo(String userName, String password) throws LoginException {
        ContentValues values = new ContentValues();

        values.put(DbHelper.TABLE_USERS_KEY_NAME, userName);
        values.put(DbHelper.TABLE_USERS_KEY_PASS, password);
        long l = mAdapter.addElement(User.class.getName(), values);

        if (l != -1) {
            new LoginException("There is already such user");
        }
    }

    @Override
    public boolean matchPassword(String userName, String password) throws LoginException {
        Cursor cursor = mAdapter.getElement(User.class.getName(), userName);

        if (cursor == null) {
            new LoginException("There is no such user");
        } else if (cursor.getString(DbHelper.TABLE_USERS_COLUMN_PASS).equals(password)) {
            return true;
        }
        return false;
    }
}

