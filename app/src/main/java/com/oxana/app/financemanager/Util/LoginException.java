package com.oxana.app.financemanager.Util;

/**
 * Created by mac on 28.07.2014.
 */
public class LoginException extends Exception {
    public LoginException(String message) {
        super(message);
    }
}
