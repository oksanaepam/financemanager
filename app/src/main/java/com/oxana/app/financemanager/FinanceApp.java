package com.oxana.app.financemanager;

import android.app.Application;

import com.oxana.app.financemanager.database.DataInCollections;
import com.oxana.app.financemanager.database.DataStore;

/**
 * Created by mac on 28.07.2014.
 */
public class FinanceApp extends Application{
    DataStore mDataBase;

    public FinanceApp(){
        super();
        DataStore dataBase = new DataInCollections();
        mDataBase = dataBase;
    }
}
