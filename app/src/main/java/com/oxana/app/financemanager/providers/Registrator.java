package  com.oxana.app.financemanager.providers;

import android.util.Log;

import  com.oxana.app.financemanager.Util.LoginException;
import  com.oxana.app.financemanager.database.DataStore;
import  com.oxana.app.financemanager.models.User;

/**
 * Created by mac on 28.07.2014.
 */
public class Registrator {
    private DataStore mDataBase;

    private final String TAG = this.getClass().toString();

    public Registrator(DataStore base) {
        mDataBase = base;
    }

    public User regUser(String name, String password) {
        try {
            mDataBase.addLogInfo(name, password);
        } catch (LoginException e) {
            Log.i(TAG, e.getMessage() + " " + name);
        } finally {
            return mDataBase.getUser(name);
        }
    }

    public User matchPassword(String userName, String password) {
        try {
            if (mDataBase.matchPassword(userName, password)) {
                Log.i(TAG, "ok");
                return mDataBase.getUser(userName);
            } else {
                Log.i(TAG, "wrong pass");
                return null;
            }
        } catch (LoginException e) {
            Log.i(TAG, e.getMessage());
            Log.i(TAG, "reg user");
            return regUser(userName, password);
        }
    }

}
