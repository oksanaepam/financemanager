package  com.oxana.app.financemanager.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Oksana_Skorniakova on 8/4/2014.
 * Класс-помошник отвечающий за создание/отктрытие
 * базы и осуществляющий контроль ее версий
 */
public class DbHelper extends SQLiteOpenHelper {
    private final String TAG = this.getClass().toString();

    public static final String TABLE_USERS = "USERS";
    public static final String TABLE_USERS_KEY_NAME = "ID";
    public static final String TABLE_USERS_KEY_PASS = "PASSWORD";

    public static final int TABLE_USERS_COLUMN_NAME = 0;
    public static final int TABLE_USERS_COLUMN_PASS = 1;

    public static final String TABLE_ACCOUNTS = "ACCOUNTS";
    public static final String TABLE_ACCOUNTS_KEY_ID = "ID";
    public static final String TABLE_ACCOUNTS_KEY_USER = "USER";
    public static final String TABLE_ACCOUNTS_KEY_BALANCE = "BALANCE";


    public static final int TABLE_ACCOUNTS_COLUMN_ID = 0;
    public static final int TABLE_ACCOUNTS_COLUMN_USER = 1;
    public static final int TABLE_ACCOUNTS_COLUMN_BALANCE = 2;
    public static final int TABLE_ACCOUNTS_COLUMN_DESCRIPTION = 3;

    public static final String TABLE_RECORDS = "RECORDS";
    public static final String TABLE_RECORDS_KEY_ID = "ID";
    public static final String TABLE_RECORDS_KEY_ACCOUNT = "ACCOUNT";
    public static final String TABLE_RECORDS_KEY_USER = "USER";
    public static final String TABLE_RECORDS_KEY_DATE = "DATE";
    public static final String TABLE_RECORDS_KEY_SUM = "SUM";
    public static final String TABLE_RECORDS_KEY_TYPE = "TYPE";
    public static final String TABLE_RECORDS_KEY_CATEGORY = "CATEGORY";
    public static final String TABLE_RECORDS_KEY_DESCRIPTION = "DESCRIPTION";

    public static final int TABLE_RECORDS_COLUMN_ID = 0;
    public static final int TABLE_RECORDS_COLUMN_ACCOUNT = 1;
    public static final int TABLE_RECORDS_COLUMN_USER = 2;
    public static final int TABLE_RECORDS_COLUMN_DATE = 3;
    public static final int TABLE_RECORDS_COLUMN_SUM = 4;
    public static final int TABLE_RECORDS_COLUMN_TYPE = 5;
    public static final int TABLE_RECORDS_COLUMN_CATEGORY = 6;
    public static final int TABLE_RECORDS_COLUMN_DESCRIPTION = 7;

    public static final String TABLE_CATEGORY = "CATEGORY";
    public static final String TABLE_CATEGORY_KEY_NAME = "ID";
    public static final String TABLE_CATEGORY_KEY_DESCRIPTION = "DESCRIPTION";

    public static final int TABLE_CATEGORY_COLUMN_ID = 0;
    public static final int TABLE_CATEGORY_COLUMN_ACCOUNT = 1;


    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    // Вызывается при создании базы на устройстве
    public void onCreate(SQLiteDatabase db) {
        // Строим стандартный sql-запрос для создания таблицы
        final String CREATE_USERS = "CREATE TABLE " + TABLE_USERS + " ("
                + TABLE_USERS_KEY_NAME + " TEXT PRIMARY KEY , "
                + TABLE_USERS_KEY_PASS + " TEXT NOT NULL);";

        final String CREATE_ACCOUNTS = "CREATE TABLE " + TABLE_ACCOUNTS + " ("
                + TABLE_ACCOUNTS_KEY_ID + " INTEGER PRIMARY KEY, "
                + TABLE_ACCOUNTS_KEY_USER + " TEXT NOT NULL"
                + TABLE_ACCOUNTS_KEY_BALANCE + " REAL);";

        final String CREATE_RECORDS = "CREATE TABLE " + TABLE_RECORDS + " ("
                + TABLE_RECORDS_KEY_ID + " INTEGER PRIMARY KEY, "
                + TABLE_RECORDS_KEY_ACCOUNT + " TEXT NOT NULL"
                + TABLE_RECORDS_KEY_USER + " TEXT NOT NULL"
                + TABLE_RECORDS_KEY_DATE + " TEXT NOT NULL"
                + TABLE_RECORDS_KEY_SUM + " REAL"
                + TABLE_RECORDS_KEY_TYPE + " INTEGER"
                + TABLE_RECORDS_KEY_CATEGORY + " INTEGER"
                + TABLE_RECORDS_KEY_DESCRIPTION + "TEXT );";

        final String CREATE_CATEGORY = "CREATE TABLE " + TABLE_CATEGORY + " ("
                + TABLE_CATEGORY_KEY_NAME + " TEXT PRIMARY KEY , "
                + TABLE_CATEGORY_KEY_DESCRIPTION + " TEXT;";

        db.execSQL(CREATE_USERS);
        db.execSQL(CREATE_ACCOUNTS);
        db.execSQL(CREATE_RECORDS);
        db.execSQL(CREATE_CATEGORY);
    }

    @Override
    // Метод будет вызван, если изменится версия базы
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        onCreate(db);
    }



}