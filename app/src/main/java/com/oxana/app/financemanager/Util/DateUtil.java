package com.oxana.app.financemanager.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mac on 28.07.2014.
 */
    public class DateUtil {

    private final String TAG=this.getClass().toString();

    public static String getDate(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        return df.format(today);
    }

    public static Date getDate(String str) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(str);
    }
}
